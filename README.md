### Show my friends in-game "played" time for a game on Steam

Very basic script + library to show all friends playtime for a specific game on Steam.

APP_ID defaults to Fallout 4.

Requires Python 3.4+ and a Steam Web API key (see start of steam-friends-played.py).

```
pip install -r requirements.txt
python3 steam-friends-played.py
```
